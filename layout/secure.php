<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

    <header role="banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?>">
        <nav role="navigation" class="navbar-inner">
            <div class="container">
                <a id="logo" href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo', 'theme'); ?>" alt="CRDF Global Logo" /></a>
                <a class="btn btn-navbar" data-toggle="workaround-collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse collapse">
                    <ul class="nav pull-right">
                        <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                    </ul>
                    <div id="tabs">
                        <?php echo $OUTPUT->custom_menu(); ?>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <div id="page-wrap" class="container">
        <div id="page">
            <header id="page-header" class="clearfix">
                <?php echo $html->heading; ?>
            </header>
            <div id="page-content" class="row-fluid">
                <div id="region-bs-main-and-pre" class="span10">
                    <div class="row-fluid">
                        <section id="region-main" class="span9 pull-right">
                            <?php echo $OUTPUT->main_content(); ?>
                        </section>
                        <?php echo $OUTPUT->blocks('side-pre', 'span3 desktop-first-column'); ?>
                    </div>
                </div>
                <?php echo $OUTPUT->blocks('side-post', 'span2'); ?>
            </div>
        </div>
    </div>

    <footer id="page-footer">
        <div id="footer-content" class="container">
            <a class="brand" href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo2', 'theme'); ?>"></a>
            <ul>
                <li><h3>&copy; 2013 CRDF Global</h3></li>
                <li>1776 Wilson Blvd, 3rd Floor - Arlington, VA 22209 USA</li>
                <li>Phone: 703-526-9720  |  E-mail: <a href="info@crdf.org">info@crdf.org</a></li>
            </ul>
            <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
        </div>
    </footer>

    <?php echo $OUTPUT->standard_end_of_body_html() ?>

</body>
</html>