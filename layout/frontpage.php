<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's crdfglobal theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_crdfglobal
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Get the HTML for the settings bits.
$html = theme_crdfglobal_get_html_for_settings($OUTPUT, $PAGE);

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css">
    </head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<header role="banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?>">
    <nav role="navigation" class="navbar-inner">
        <div class="container">
            <a id="logo" href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo', 'theme'); ?>" alt="CRDF Global Logo" /></a>
            <a class="btn btn-navbar" data-toggle="workaround-collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse collapse">
                <ul class="nav pull-right">
                    <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                    <li class="navbar-text"><?php echo $OUTPUT->login_info() ?></li>
                </ul>
                <div id="tabs">
                    <?php echo $OUTPUT->custom_menu(); ?>
                </div>
            </div>
        </div>
    </nav>
</header>

<div id="page-wrap" class="container">
    <div id="layerslider-container">
        <div id="layerslider" style="width: 1170px; height: 300px; margin: 0px auto; ">
            <div class="ls-layer" style="transition2d: 5; slideDelay : 5000;"> 
                <img src="<?php echo $OUTPUT->pix_url('banner3', 'theme'); ?>" class="ls-bg" alt="Slide background" style="slidedirection : fade; slideoutdirection : fade; ">
                <p class="ls-s2 l4-s2" style="top:69px; left: 100px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    Commercialization Pathfinder Program
                </p>
                <p class="ls-s8 l4-s2" style="top:113px; left: 100px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    Your Guide From Idea to Market
                </p>
                <p class="ls-s14 l4-s3" style="top:189px; left: 100px; slidedirection : left; slideoutdirection : left;"><a href="<?php echo $CFG->wwwroot ?>/mod/page/view.php?id=14" class="btn">Learn more about the program...</a></p>
            </div>
            <div class="ls-layer" style="transition2d: 5; slideDelay : 7000">
                <img src="<?php echo $OUTPUT->pix_url('banner2', 'theme'); ?>" class="ls-bg" alt="Slide background" style="slidedirection : fade; slideoutdirection : fade; ">
                <p class="ls-s2 l4-s1" style="top:59px; left: 42px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    “The unique advantage of this course comes from
                </p>
                <p class="ls-s6 l4-s1" style="top:97px; left: 42px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    the personal entrepreneurial experience of the participating mentors.
                </p>
                <p class="ls-s10 l4-s1" style="top:135px; left: 42px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    Such real life applicable information is very valuable, thank you.”
                </p>
                <p class="ls-s14 l4-s1 l4-name" style="top:173px; left: 42px; slidedirection : left; slideoutdirection : left;">
                    -Denis Kornilov, AkkoLab
                </p>
                <p class="ls-s14 l4-s3" style="top:221px; left: 42px; slidedirection : left; slideoutdirection : left;"><a href="<?php echo $CFG->wwwroot ?>/mod/page/view.php?id=15" class="btn">Learn more about our results and success stories...</a></p>
            </div>
            <div class="ls-layer" style="transition2d: 5; slideDelay : 7000">
                <img src="<?php echo $OUTPUT->pix_url('banner1', 'theme'); ?>" class="ls-bg" alt="Slide background" style="slidedirection : fade; slideoutdirection : fade; ">
                <p class="ls-s2 l4-s1" style="top:59px; left: 42px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    “Through participation in the program,
                </p>
                <p class="ls-s6 l4-s1" style="top:97px; left: 42px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    I was able to improve my project and start a company
                </p>
                <p class="ls-s10 l4-s1" style="top:135px; left: 42px; slidedirection : left; slideoutdirection : left;durationin : 1000;">
                    which we are now developing and taking to market.”
                </p>
                <p class="ls-s14 l4-s1 l4-name" style="top:173px; left: 42px; slidedirection : left; slideoutdirection : left;">
                    -Aleksandr Kolbas
                </p>
                <p class="ls-s14 l4-s3" style="top:221px; left: 42px; slidedirection : left; slideoutdirection : left;"><a href="<?php echo $CFG->wwwroot ?>/mod/page/view.php?id=18" class="btn">Learn about our current news and trainings...</a></p>
            </div>
        </div>
    </div>

    <div id="page">
        <header id="page-header" class="clearfix">
            <div id="page-navbar" class="clearfix">
                <div class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></div>
                <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
            </div>
            <?php echo $html->heading; ?>
            <div id="course-header">
                <?php echo $OUTPUT->course_header(); ?>
            </div>
        </header>

        <div id="page-content" class="row-fluid">
            <div id="<?php echo $regionbsid ?>" class="span10">
                <div class="row-fluid">
                    <section id="region-main" class="span9 pull-right">
                        <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                        ?>
                    </section>
                    <?php echo $OUTPUT->blocks('side-pre', 'span3 desktop-first-column'); ?>
                </div>
            </div>
            <?php echo $OUTPUT->blocks('side-post', 'span2'); ?>
        </div>
    </div>
</div>

<footer id="page-footer">
    <div id="footer-content" class="container">
        <a class="brand" href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo2', 'theme'); ?>"></a>
        <ul>
            <li><h3>&copy; 2013 CRDF Global</h3></li>
            <li>1776 Wilson Blvd, 3rd Floor - Arlington, VA 22209 USA</li>
            <li>Phone: 703-526-9720  |  E-mail: <a href="info@crdf.org">info@crdf.org</a></li>
        </ul>
        <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
    </div>
</footer>

<?php echo $OUTPUT->standard_end_of_body_html() ?>

</body>
</html>
