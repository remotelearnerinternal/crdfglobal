$(document).ready(function(){
    $('#layerslider').layerSlider({
        skinsPath : '', // No path so we don't have a failed get on init.
        skin : '', // Skin CSS is loaded via config.php
        hoverPrevNext : true,
        navPrevNext : true,
        pauseOnHover : false,
        responsive : true
    });
});