<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_crdfglobal', language 'en'
 *
 * @package   theme_crdfglobal
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>CRDF Global</h2>
<h3>About</h3>
<p>crdfglobal is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
</div></div>';

$string['configtitle'] = 'crdfglobal';
$string['pluginname'] = 'CRDF Global';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
